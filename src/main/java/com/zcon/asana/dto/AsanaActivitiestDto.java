package com.zcon.asana.dto;

import java.io.File;

public class AsanaActivitiestDto {

	private String bearerToken;
	private String workspace;
	private String name;
	private String notes;
	private String projects;
	
	private String userName;
	private String password;
	
	private String taskId;
	private String file;
	
	public String getBearerToken() {
		return bearerToken;
	}
	public void setBearerToken(String bearerToken) {
		this.bearerToken = bearerToken;
	}
	public String getWorkspace() {
		return workspace;
	}
	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getProjects() {
		return projects;
	}
	public void setProjects(String projects) {
		this.projects = projects;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	@Override
	public String toString() {
		return "AsanaActivitiestDto [bearerToken=" + bearerToken + ", workspace=" + workspace + ", name=" + name
				+ ", notes=" + notes + ", projects=" + projects + ", userName=" + userName + ", password=" + password
				+ ", taskId=" + taskId + ", file=" + file + "]";
	}

	

	

}

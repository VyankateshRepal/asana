package com.zcon.asana.dao.implementations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.zcon.asana.dao.interfaces.IAsanaActivitiesDao;
import com.zcon.asana.dto.AsanaActivitiestDto;
import com.zcon.asana.dto.SearchParametersDto;
import com.zcon.asana.exceptions.SpringAppRuntimeException;

/**
 * @author Vyankatesh
 *
 */
@SuppressWarnings({ "deprecation", "unused" })
@Repository("blobSearchServiceDao")
public class AsanaActivitiesDaoImpl implements IAsanaActivitiesDao {

	@Override
	public JSONObject getAccountInformation(AsanaActivitiestDto asanaActivitiesDto) {
		JSONObject jsonOutput = null;
		try {
			String url = "https://app.asana.com/api/1.0/users/me";
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
			HttpResponse response = client.execute(get);
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			System.out.println(result.toString());
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONObject createNewProject(AsanaActivitiestDto asanaActivitiesDto) {
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(asanaActivitiesDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);

			String url = "https://app.asana.com/api/1.0/projects";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
			// add required parameters to API
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("workspace", jsonInput.get("workspace").toString()));
			urlParameters.add(new BasicNameValuePair("name", jsonInput.get("name").toString()));
			urlParameters.add(new BasicNameValuePair("notes", jsonInput.get("notes").toString()));

			jsonOutput = getPostDataOutput(url, client, post, urlParameters);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONObject createNewTask(AsanaActivitiestDto asanaActivitiesDto) {
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(asanaActivitiesDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);

			String url = "https://app.asana.com/api/1.0/tasks";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
			// add required parameters to API
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("workspace", jsonInput.get("workspace").toString()));
			urlParameters.add(new BasicNameValuePair("name", jsonInput.get("name").toString()));
			urlParameters.add(new BasicNameValuePair("notes", jsonInput.get("notes").toString()));
			urlParameters.add(new BasicNameValuePair("projects", jsonInput.get("projects").toString()));

			jsonOutput = getPostDataOutput(url, client, post, urlParameters);
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	/**
	 * @param url
	 * @param client
	 * @param post
	 * @param urlParameters
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws UnsupportedOperationException
	 * @throws ParseException
	 */
	private JSONObject getPostDataOutput(String url, HttpClient client, HttpPost post,
			List<NameValuePair> urlParameters) throws UnsupportedEncodingException, IOException,
			ClientProtocolException, UnsupportedOperationException, ParseException {
		JSONObject jsonOutput;
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println(result.toString());
		JSONParser parser = new JSONParser();
		jsonOutput = (JSONObject) parser.parse(result.toString());
		return jsonOutput;
	}

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	@Override
	public JSONObject getProjectsList(AsanaActivitiestDto asanaActivitiesDto) {
		JSONObject jsonOutput = null;
		try {
			String url = "https://app.asana.com/api/1.0/projects";
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
			HttpResponse response = client.execute(get);
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			System.out.println(result.toString());
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}

	@Override
	public JSONObject createNewTaskFromMail(AsanaActivitiestDto asanaActivitiesDto) {
		String host = "pop.secureserver.net";// change accordingly
		String mailStoreType = "pop3";
		String userName = asanaActivitiesDto.getUserName();// change accordingly
		String password = asanaActivitiesDto.getPassword();// change accordingly
		JSONObject jsonOutput = new JSONObject();

		try {
			
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(asanaActivitiesDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);

			// create properties field
			Properties properties = new Properties();

			properties.put("mail.pop3.host", host);
			properties.put("mail.pop3.port", "110");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			javax.mail.Store store = emailSession.getStore("pop3s");

			store.connect(host, userName, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			javax.mail.Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);
			if (messages.length > 0) {
				for (int i = 0, n = messages.length; i < n; i++) {
					javax.mail.Message message = messages[i];
					System.out.println("---------------------------------");
					System.out.println("Email Number " + (i + 1));
					System.out.println("Subject: " + message.getSubject());
					System.out.println("From: " + message.getFrom()[0]);
					System.out.println("Text: " + message.getContent().toString());

					if (message.getSubject().toString().equalsIgnoreCase("resume")
							|| message.getSubject().toString().equalsIgnoreCase("cv")
							|| message.getSubject().toString().equalsIgnoreCase("biodata")) {
						
						try {
							String url = "https://app.asana.com/api/1.0/tasks";
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost(url);
							post.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
							// add required parameters to API
							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters
									.add(new BasicNameValuePair("workspace", jsonInput.get("workspace").toString()));
							urlParameters.add(new BasicNameValuePair("name",
									"tasks for  " + message.getSubject() + "  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("notes",
									"define tasks related to person having mail:  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("projects", jsonInput.get("projects").toString()));

							jsonOutput = getPostDataOutput(url, client, post, urlParameters);
						} catch (Exception e) {
							e.printStackTrace();
							String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
							throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
						}
					}
				}
			}
			
			else{
				jsonOutput = (JSONObject) jsonOutput.put("result", "no new mails found");
			}
			// close the store and folder objects
			emailFolder.close(false);
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonOutput;
	}

	@Override
	public JSONObject addAttachmentToTask(AsanaActivitiestDto asanaActivitiesDto) {
		JSONObject jsonOutput = new JSONObject();
		try {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(asanaActivitiesDto);
			org.json.JSONObject jsonInput = new org.json.JSONObject(json);
			String url = "https://app.asana.com/api/1.0/tasks/"+asanaActivitiesDto.getTaskId()+"/attachments";
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", "Bearer " + asanaActivitiesDto.getBearerToken());
		
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addTextBody("task", jsonInput.get("taskId").toString(), ContentType.TEXT_PLAIN);
//			File f = new File(jsonInput.get("file").toString());
			File f = new File(asanaActivitiesDto.getFile());
			builder.addBinaryBody(
			    "file",
			    new FileInputStream(f),
			    ContentType.APPLICATION_OCTET_STREAM,
			    f.getName()
			);
			HttpEntity multipart = builder.build();
			post.setEntity(multipart);
			HttpResponse response = client.execute(post);
			HttpEntity responseEntity = response.getEntity();
			
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			System.out.println(result.toString());
			JSONParser parser = new JSONParser();
			jsonOutput = (JSONObject) parser.parse(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
			throw new SpringAppRuntimeException(errorMessageAndClassWithMethod + e.toString());
		}
		return jsonOutput;
	}
}
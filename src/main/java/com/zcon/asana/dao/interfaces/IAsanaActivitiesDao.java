package com.zcon.asana.dao.interfaces;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.zcon.asana.dto.AsanaActivitiestDto;
import com.zcon.asana.dto.SearchParametersDto;


/**
 * @author Vyankatesh
 *
 */
public interface IAsanaActivitiesDao {

	public JSONObject getAccountInformation(AsanaActivitiestDto asanaActivitiesDto);

	public JSONObject createNewProject(AsanaActivitiestDto asanaActivitiesDto);

	public JSONObject createNewTask(AsanaActivitiestDto asanaActivitiesDto);

	public JSONObject getProjectsList(AsanaActivitiestDto asanaActivitiesDto);

	public JSONObject createNewTaskFromMail(AsanaActivitiestDto asanaActivitiesDto);

	public JSONObject addAttachmentToTask(AsanaActivitiestDto asanaActivitiesDto);

}

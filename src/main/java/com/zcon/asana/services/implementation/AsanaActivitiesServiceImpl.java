package com.zcon.asana.services.implementation;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcon.asana.dao.interfaces.IAsanaActivitiesDao;
import com.zcon.asana.dto.AsanaActivitiestDto;
import com.zcon.asana.dto.SearchParametersDto;
import com.zcon.asana.services.interfaces.IAsanaActivitiesServices;

/**
 * @author Vyankatesh
 *
 */
@Service("blobSearchService")
public class AsanaActivitiesServiceImpl implements IAsanaActivitiesServices {

	@Autowired
	IAsanaActivitiesDao asanaActivitiesServiceDao;

	@Override
	public JSONObject getAccountInformation(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.getAccountInformation( asanaActivitiesDto);
	}

	@Override
	public JSONObject createNewProject(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.createNewProject( asanaActivitiesDto);
	}

	@Override
	public JSONObject createNewTask(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.createNewTask( asanaActivitiesDto);
	}

	@Override
	public JSONObject getProjectsList(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.getProjectsList(asanaActivitiesDto);
	}

	@Override
	public JSONObject createNewTaskFromMail(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.createNewTaskFromMail(asanaActivitiesDto);
	}

	@Override
	public JSONObject addAttachmentToTask(AsanaActivitiestDto asanaActivitiesDto) {
		// TODO Auto-generated method stub
		return asanaActivitiesServiceDao.addAttachmentToTask(asanaActivitiesDto);
	}



}

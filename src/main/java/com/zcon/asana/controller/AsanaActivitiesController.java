package com.zcon.asana.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zcon.asana.dto.AsanaActivitiestDto;
import com.zcon.asana.services.interfaces.IAsanaActivitiesServices;

/**
 * @author Vyankatesh
 *
 */
@RestController
@RequestMapping("/activities")
public class AsanaActivitiesController {
	@Autowired
	IAsanaActivitiesServices asanaActivitiesService;
	DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	Calendar calobj = Calendar.getInstance();

	
	@RequestMapping(value = "/getAccountInformation", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getAccountInformation(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.getAccountInformation(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createNewProject", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createNewProject(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.createNewProject(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createNewTask", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createNewTask(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.createNewTask(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createNewTaskFromMail", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createNewTaskFromMail(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.createNewTaskFromMail(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProjectsList", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> getProjectsList(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.getProjectsList(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addAttachmentToTask", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> addAttachmentToTask(@RequestBody AsanaActivitiestDto asanaActivitiesDto) throws Exception {
		JSONObject result = asanaActivitiesService.addAttachmentToTask(asanaActivitiesDto);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	/*@RequestMapping(value = "/listDatasources", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> listDatasources() throws NumberFormatException, Exception {
		JSONObject result = asanaActivitiesService.listDatasources();
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/createDatasource", method = RequestMethod.POST)
	public ResponseEntity<JSONObject> createDatasource(@RequestBody JSONObject jsonInput) throws Exception {
		JSONObject result = asanaActivitiesService.createDatasource(jsonInput);
		if (result == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}*/

}

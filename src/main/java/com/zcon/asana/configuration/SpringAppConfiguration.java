package com.zcon.asana.configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.zcon.asana.filter.CORSFilter;

/**
 * @author Vyankatesh
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.zcon.asana")
public class SpringAppConfiguration extends WebMvcConfigurerAdapter {
	/**
	 * Configure ViewResolvers to deliver preferred views.
	 */
	@Bean
	public CORSFilter corsFilter() {
		CORSFilter corsFilter = new CORSFilter();
		return corsFilter;
	}
}
